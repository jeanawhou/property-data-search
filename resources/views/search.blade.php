<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <title>House plan search</title>

       
        <style>
        </style>
    </head>
    <body>
        <div class="container">
            <form method="POST" action="" class="">
                
                <div class="form-group col-xs-2 col-md-2"> 
                    <label for="name">Name:</label>
                    <input id="name" name="name" type="text" class="form-control"/>
                </div>
                <div class="form-group col-xs-2 col-md-2">
                    <label>Price:</label>
                    <input id="from" name="from" type="text" class="form-control " placeholder="Staring price" />
                    <input id="to" name="to" type="text" class="form-control" placeholder="Last price" />
                </div>
                <div class="form-group col-xs-1 col-md-1">
                    <label for="bed">Bedrooms:</label>
                    <input id="bed" name="bed" type="text" class="form-control"/>
                </div>
                <div class="form-group col-xs-1 col-md-1">
                    <label for="bath">Bathroom:</label>
                    <input id="bath" name="bath" type="text" class="form-control"/>
                </div>
                
                <div class="form-group col-xs-1 col-md-1">
                    <label for="storey">Storeys:</label>
                    <input id="storey" name="storey" type="text" class="form-control"/>
                </div>
                <div class="form-group col-xs-1 col-md-1">
                    <label for="garage">Garage:</label>
                    <input id="garage" name="garage" type="text" class="form-control"/>
                </div>
                
                <div class="form-group col-xs-2 col-md-2">
                    <input type="submit" value="Search" class="form-control"/>
                </div>
                
            </form> 

            <div id="table_div">
                <table id="display_table" class="table-bordered">
                    <thead>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Bedrooms</th>
                        <th>Bathrooms</th>
                        <th>Storey</th>
                        <th>Garage</th>
                            
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

        </div>

        
    </body>

    <script type="text/javascript"> 

        $(function(){

            $("input").keyup(function(){ 

                var propName = $("#name").val();
                var propPrice = $("#price").val();
                var propBed = $("#bed").val();
                var propBath = $("#bath").val();
                var propStorey = $("#storey").val();
                var propGarage = $("#garage").val(); 

                var propData = {
                    prop_name: $("#name").val(),
                    prop_price_from: $("#from").val(),
                    prop_price_to: $("#to").val(),
                    prop_bed: $("#bed").val(),
                    prop_bath: $("#bath").val(),
                    prop_storey: $("#storey").val(),
                    prop_garage: $("#garage").val(), 
                }; 

            

                $.ajax({
                    url: '/search-ajax/',
                    data: propData,
                    dataType: 'json',
                    method: 'get',
                
                    success: function (response) {   

                        displayResults(response.property);               
                    }
                
                });
            });
        }); 

        function displayResults(values){
            
                var display_table = $("#display_table tbody");
                display_table.empty();

                $.each(values, function(idx, elem){
                    display_table.append("<tr><td>"+elem.Name
                    +"</td><td>"+elem.Price
                    +"</td><td>"+elem.Bedrooms
                    +"</td><td>"+elem.Bathrooms
                    +"</td><td>"+elem.Storeys
                    +"</td><td>"+elem.Garages
                    +"</td></tr>");
            });
        }

    </script>
</html>
