<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB; 
class SearchController extends Controller { 


	public function show(Request $request){

        $propName = (string) $request->input('prop_name');
        $propFrom = (float) $request->input('prop_price_from');         
        $propTo = (float) $request->input('prop_price_to');         
        $propBeds = (int) $request->input('prop_bed');
        $propBaths = (int) $request->input('prop_bath'); 
		$propStoreys = (int) $request->input('prop_storey');
        $propGarages = (int) $request->input('prop_garage'); 




        $query = DB::table("property_data")->select("*");
        $query = empty($propName) ? $query : $query->where("Name", "LIKE", "%$propName%");
        $query = empty($propBeds) ? $query : $query->where("Bedrooms", "=", "$propBeds");
        $query = empty($propBaths) ? $query : $query->where("Bathrooms", "=", "$propBaths");
        $query = empty($propStoreys) ? $query : $query->where("Storeys", "=", "$propStoreys");
        $query = empty($propGarages) ? $query : $query->where("Garages", "=", "$propGarages");
        $query = empty($propFrom) || empty($propTo) ? $query : $query->whereBetween("Price", [$propFrom, $propTo]);
        
       	$query = $query->get(); 

        // dd($query);
        
        return response()->json([
            "property" => $query
            ]);
    } 

}
